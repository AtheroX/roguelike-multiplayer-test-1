using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerControllerMulti : NetworkBehaviour {

	public float xSpeed;
	public float zSpeed;
	public float runspeed;

	public GameObject bulletPrefab;
	public float ShootSpeed = 3.0f;
	public float ShootSpeedRecarga;
	public float shootforce;
	public bool Disparado = false;
	public bool UP = false;
	public bool DOWN = false;
	public bool LEFT = false;
	public bool RIGHT = false;
	public bool RUN = false;
	public GameObject zscripts;
	public PlayerControllerHelper pch;
	public camara cam;

	void Start(){
		ShootSpeedRecarga = ShootSpeed;
		zscripts = GameObject.Find ("0scripts");
		/*pch=zscripts.GetComponent<PlayerControllerHelper> ();
		pch.miPlayer = this.gameObject;
		cam = pch.micamara.GetComponent<camara> ();
		cam.player_transform = this.gameObject;
		pch.InstantiateStuff ();*/
		if (runspeed == 0) {
			runspeed = 2;
		}
	}
	void Update(){

		if (!isLocalPlayer) {
			return;
		}

		if(Disparado){
			ShootSpeed -= Time.deltaTime;
		}
		if (ShootSpeed <= 0) {
			ShootSpeed = ShootSpeedRecarga;
			Disparado = false;
			UP = false;
			DOWN = false;
			RIGHT = false;
			LEFT = false;
			Debug.Log ("YEP");
		}
		if (!RUN) {
			var x = Input.GetAxis ("Horizontal") * Time.deltaTime * xSpeed;
			var z = Input.GetAxis ("Vertical") * Time.deltaTime * zSpeed;
			transform.Translate(x, 0, 0);
			transform.Translate(0, 0, z);
		} else {
			var x = Input.GetAxis ("Horizontal") * Time.deltaTime * (xSpeed * runspeed);
			var z = Input.GetAxis ("Vertical") * Time.deltaTime * (zSpeed * runspeed);
			transform.Translate(x, 0, 0);
			transform.Translate(0, 0, z);
		}

		if (Input.GetKey(KeyCode.LeftShift)) {
			RUN = true;
		}else
		{RUN = false;
		}

		if (UP) {
			DOWN = false;
			RIGHT = false;
			LEFT = false;
		}
		if (DOWN) {
			UP = false;
			RIGHT = false;
			LEFT = false;
		}
		if (RIGHT) {
			DOWN = false;
			UP = false;
			LEFT = false;
		}
		if (LEFT) {
			DOWN = false;
			RIGHT = false;
			UP = false;
		}

		if ((Input.GetKey (KeyCode.UpArrow)) && (ShootSpeed == ShootSpeedRecarga)&& ((!UP)&&(!DOWN)&&(!LEFT)&&(!RIGHT))) {
			CmdDisparadoAdelante ();
			Disparado = true;
			UP = true;
		} 
		if ((Input.GetKey (KeyCode.RightArrow)) && (ShootSpeed == ShootSpeedRecarga)&& ((!RIGHT)&&(!LEFT)&&(!UP)&&(!DOWN))) {
			CmdDisparadoDerecha ();
			Disparado = true;
			RIGHT = true;
		}
		if ((Input.GetKey (KeyCode.DownArrow)) && (ShootSpeed == ShootSpeedRecarga) && (!DOWN)&&(!UP)&&(!RIGHT)&&((!LEFT))) {
			CmdDisparadoAtras ();
			Disparado = true;
			DOWN = true;
		} 
		if ((Input.GetKey (KeyCode.LeftArrow)) && (ShootSpeed == ShootSpeedRecarga)&& ((!LEFT)&&(!RIGHT)&&(!DOWN)&&(!UP))) {
			CmdDisparadoIzquierda ();
			Disparado = true;
			LEFT = true;
		}
	//	idee =(int) this.GetComponent<NetworkIdentity> ().netId;
	}
	[Command]
	void CmdDisparadoAdelante(){

		var bullet = (GameObject)Instantiate (bulletPrefab, new Vector3(this.transform.position.x,this.transform.position.y+1,this.transform.position.z+1), new Quaternion (this.gameObject.transform.rotation.x,this.gameObject.transform.rotation.y,this.gameObject.transform.rotation.z,0));
		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * shootforce;
		NetworkServer.Spawn(bullet);
	
	}
	[Command]
	void CmdDisparadoAtras(){

		var bullet = (GameObject)Instantiate (bulletPrefab, new Vector3(this.transform.position.x,this.transform.position.y+1,this.transform.position.z-1), new Quaternion(this.gameObject.transform.rotation.x,this.gameObject.transform.rotation.y-180,this.gameObject.transform.rotation.z,0));
		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * shootforce;
		NetworkServer.Spawn(bullet);

	}
	[Command]
	void CmdDisparadoDerecha(){

		var bullet = (GameObject)Instantiate (bulletPrefab, new Vector3(this.transform.position.x+1,this.transform.position.y+1,this.transform.position.z), new Quaternion(this.gameObject.transform.rotation.x,this.gameObject.transform.rotation.y,this.gameObject.transform.rotation.z,0));
		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.right * shootforce;
		NetworkServer.Spawn(bullet);

	}
	[Command]
	void CmdDisparadoIzquierda(){

		var bullet = (GameObject)Instantiate (bulletPrefab, new Vector3(this.transform.position.x-1,this.transform.position.y+1,this.transform.position.z), new Quaternion(this.gameObject.transform.rotation.x,this.gameObject.transform.rotation.y-90,this.gameObject.transform.rotation.z,0));
		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.right * shootforce;
		NetworkServer.Spawn(bullet);
	}

	public override void OnStartLocalPlayer(){

		GetComponent<MeshRenderer> ().material.color = Color.green;

		//miHUD.GetComponent<Hud> ().setTargetGO (this.gameObject);
	}
}