﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float xSpeed;
	public float zSpeed;

	void Update () {
	
		var x = Input.GetAxis ("Horizontal") * Time.deltaTime * xSpeed;
		var z = Input.GetAxis ("Vertical") * Time.deltaTime * zSpeed;
		transform.Translate(x, 0, 0);
		transform.Translate(0, 0, z);

	}
}
