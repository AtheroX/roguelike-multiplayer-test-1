﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Health : NetworkBehaviour {

	public const float maxHealth = 100*3.232f;
	public const float maxMana = 100*3.232f;
	public bool destroyOnDeath;

	[SyncVar(hook = "OnChangeHealth")]
	public float currentHealth = maxHealth;
	public float currentMana = maxMana;

	public RectTransform healthBar;
	public RectTransform manaBar;


	private NetworkStartPosition[] spawnPoints;

	void Start ()
	{
		if (isLocalPlayer)
		{
			spawnPoints = FindObjectsOfType<NetworkStartPosition>();
		}

		healthBar = Hud.HealthBar;
		manaBar = Hud.ManaBar;
	}

	public void TakeDamage(float amount)
	{
		if (!isServer)
			return;

		currentHealth -= amount;
		if (currentHealth <= 0)
		{
			if (destroyOnDeath)
			{
				Destroy(gameObject);
			} 
			else
			{
				currentHealth = maxHealth;
				// called on the Server, invoked on the Clients
				RpcRespawn();
			}
		}
	}

	void OnChangeHealth (float currentHealth )
	{
		Debug.Log ("NOPE");
		healthBar.sizeDelta = new Vector2(currentHealth , healthBar.sizeDelta.y);
	}

	[ClientRpc]
	void RpcRespawn()
	{
		if (isLocalPlayer)
		{
			// Set the spawn point to origin as a default value
			Vector3 spawnPoint = Vector3.zero;

			// If there is a spawn point array and the array is not empty, pick one at random
			if (spawnPoints != null && spawnPoints.Length > 0)
			{
				spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
			}

			// Set the player’s position to the chosen spawn point
			transform.position = spawnPoint;
		}
	}
}