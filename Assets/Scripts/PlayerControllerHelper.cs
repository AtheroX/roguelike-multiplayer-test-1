﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerHelper : MonoBehaviour {

	public GameObject micamara;
	public GameObject seguimiento;
	public GameObject miHud; 
	public GameObject miPlayer;

	// Use this for initialization
	void Start () {

		micamara = this.transform.FindChild ("Camarita").gameObject;
		micamara.SetActive (true);
		miHud = this.transform.FindChild ("HUD").gameObject;
		miHud.SetActive (true);
		miPlayer = this.transform.FindChild ("Player").gameObject;
		miPlayer.SetActive (true);
		micamara.GetComponent<camara>().player_transform = miPlayer.gameObject;
	}
	
	// Update is called once per frame
	void Update () {

		seguimiento = miPlayer.gameObject;
	}
}
