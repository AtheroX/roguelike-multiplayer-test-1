﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System;

public class Habilidades : NetworkBehaviour {

	public ParticleSystem[] habilidad;
	public ParticleSystem[] MyHab;
	public float[] consumoMana;
	public float[] Damage;
	public float Duracion;
	public Habilidad Habilidad_sc;


	void Start () {
		if (isLocalPlayer) {
			ParticleSystem Hab0 = Instantiate (habilidad [0]);
			Hab0.gameObject.transform.parent = this.transform;
			habilidad [0] = Hab0;
			var ParaMyPlayer0 = GameObject.Find ("Beta1_Hab0(Clone)");
			Habilidad_sc = ParaMyPlayer0.GetComponent<Habilidad> ();
			Habilidad_sc.myPlayer = this.gameObject;
		}
	}
	
	void Update () {
		if(isLocalPlayer){
			var ManaDePj = this.gameObject.GetComponent<ComponentesDelJugador> ();
			MyHab[0] = GameObject.Find("Beta1_Hab0(Clone)").GetComponent<ParticleSystem>();
			if ((Input.GetKeyDown (KeyCode.Alpha1)) && (ManaDePj.currentMana >= consumoMana [0]) && (!MyHab [0].isPlaying) && (ManaDePj != null)) {
				MyHab [0].Play ();
				Debug.Log ("Si");
				ManaDePj.UseMana (consumoMana [0]); 
			}
		}
	}
}