﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;

public class Bala : NetworkBehaviour {

	public GameObject myPlayer;
	[SyncVar]
	public float Damage;
	[SyncVar]
	public float AutoDestruct;
	// Use this for initialization
	void Start () {
		if (myPlayer != null) {
			Damage = myPlayer.GetComponent<ComponentesDelJugador> ().DañoFisico;
		} else {
			Damage = 10 * 3.232f;
		}
	}
	
	// Update is called once per frame
	void Update () {
		AutoDestruct = AutoDestruct - Time.deltaTime;
		if (AutoDestruct <= 0) {
			Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter(Collider other){

		var hit = other.gameObject;
		var health = hit.GetComponent<ComponentesDelJugador> ();
		//recuerda poner el && (other.gameObject.tag != "Player")
		if ((other.gameObject.name != "Bullet")) {
			Destroy (this.gameObject);
		}
		if (health != null) {
			health.TakeDamage (Damage);
		}

	}
}