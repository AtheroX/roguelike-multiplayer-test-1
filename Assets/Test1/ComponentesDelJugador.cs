﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ComponentesDelJugador : NetworkBehaviour {

	public const float maxVida = 100*3.232f;
	public const float maxMana = 100*3.232f;

	[SyncVar(hook = "OnChangeHealth")]
	public float currentVida = maxVida;
	[SyncVar(hook = "OnChangeMana")]
	public float currentMana = maxMana;

	public float VidaRegen;
	public float ManaRegen;

	public RectTransform HealthBar;
	public RectTransform ManaBar;

	public float DañoFisico;
	public float DañoMagico;

	public int Nivel;
	public float exp;

	public float timer1sec = 1.0f;

	// Use this for initialization
	void Start () {
		if (isLocalPlayer) {
			RectTransform[] Bars = FindObjectsOfType (typeof(RectTransform)) as RectTransform[];
			Debug.Log ("Bar 0 " + Bars[0].name + " Bar 1 " + Bars[1].name + " Bar 2 " + Bars[2].name + " Bar 3 " + Bars[3].name);
			if (Bars [3].name == "HealthBar") {
				HealthBar = Bars [3];
			}

			if (Bars [2].name == "ManaBar") {
				ManaBar = Bars [2];
			}


			if (DañoFisico == 0) {
				DañoFisico = 10*3.232f;
			}
			if (DañoMagico == 0) {
				DañoMagico = 10*3.232f;
			}
		}
	}
	public void TakeDamage(float amount){
		if(isLocalPlayer){
		currentVida -= amount;
			if (currentVida <= 0) {
			
				currentVida = maxVida;
				currentMana = maxMana;
				RpcRespawn ();
			}
		}
	}
	public void UseMana(float amount){
		if (isLocalPlayer) {

			currentMana -= amount;
		}
	}

	void OnChangeHealth (float currentVida ){
		
		Debug.Log ("CambioEnVida");
		HealthBar.sizeDelta = new Vector2(currentVida , HealthBar.sizeDelta.y);
	}

	void OnChangeMana (float currentMana){
		
		Debug.Log ("CombioEnMana");
		ManaBar.sizeDelta = new Vector2(currentMana , HealthBar.sizeDelta.y);
	}
	
	void Update () {
		if (isLocalPlayer) {
			timer1sec -= Time.deltaTime;
			if (timer1sec <= 0) {
				timer1sec = 1;
			}
			if ((currentVida != maxVida) && (timer1sec == 1)) {
				TakeDamage (-VidaRegen);
			}
			if ((currentMana != maxMana) && (timer1sec == 1)) {
				UseMana (-ManaRegen);
			}
			if (currentVida > maxVida) {
				currentVida = maxVida;
			}
			if (currentMana > maxMana) {
				currentMana = maxMana;
			}
		}
	}

	[ClientRpc]
	void RpcRespawn()
	{
		if (isLocalPlayer)
		{
			Vector3 spawnPoint = Vector3.zero;
			transform.position = spawnPoint;
		}
	}
}
