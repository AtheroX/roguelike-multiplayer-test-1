﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Habilidad : MonoBehaviour {

	public GameObject myPlayer;
	public float DañoFisico;
	public float DañoMagico;
	public bool Fisico = false;
	// Use this for initialization
	void Start () {
		DañoFisico = myPlayer.GetComponent<ComponentesDelJugador> ().DañoFisico;
		DañoMagico = myPlayer.GetComponent<ComponentesDelJugador> ().DañoMagico;

		if (this.gameObject.name == "Beta1_Hab0(Clone)") {
			Fisico = false;
		}
	}
	
	// Update is called once per frame
	void Update () {



	}
	void OnTriggerEnter(Collider other){

		var hit = other.gameObject;
		var health = hit.GetComponent<ComponentesDelJugador> ();
		//recuerda poner el && (other.gameObject.tag != "Player")
		if (health != null) {
			if (!Fisico) {
				health.TakeDamage (DañoMagico);
			}
		}
	}
}