﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;

public class ControlPJ : NetworkBehaviour {

	public float Xspeed;
	public float Zspeed;
	public float Runspeed;

	public GameObject BulletPref;
	public float ShootSpeed = 3.0f;
	public float ShootSpeedRecarga;
	public float shootforce;
	public bool Disparado = false;
	public bool UP = false;
	public bool DOWN = false;
	public bool LEFT = false;
	public bool RIGHT = false;
	public bool RUN = false;


	public GameObject MiCamara;
	public GameObject MiCamaraReal;
	public GameObject MiHud;
	public GameObject MiHudReal;
	public GameObject Flecha;
	public GameObject FlechaReal;

	public int clase;

	public Bala Bala_sc;


	void Start () {
		if (isLocalPlayer) {
			
			Image flecha_img = Flecha.GetComponentInChildren<Image>();
			flecha_img.color = new Color (0f,255f,255f, 1f);
				
			MiCamaraReal = Instantiate (MiCamara) as GameObject;
			MiHudReal = Instantiate (MiHud) as GameObject;
			FlechaReal = Instantiate (Flecha) as GameObject;


			MiCamaraReal.transform.parent = this.gameObject.transform;
			MiHudReal.transform.parent = this.gameObject.transform;
			FlechaReal.transform.parent = this.gameObject.transform;


		}
		ShootSpeedRecarga = ShootSpeed;
		if (Runspeed == 0) {
			Runspeed = 2;
		}
		if (Xspeed == 0) {
			Xspeed = 15;
		}
		if (Zspeed == 0) {
			Zspeed = 15;
		}

	}

	// Update is called once per frame
	void Update () {
		if (isLocalPlayer) {
			if (Disparado) {
				ShootSpeed -= Time.deltaTime;
			}
			if (ShootSpeed <= 0) {
				ShootSpeed = ShootSpeedRecarga;
				Disparado = false;
				UP = false;
				DOWN = false;
				RIGHT = false;
				LEFT = false;
				Debug.Log ("YEP");
			}
			if (!RUN) {
				var x = Input.GetAxis ("Horizontal") * Time.deltaTime * Xspeed;
				var z = Input.GetAxis ("Vertical") * Time.deltaTime * Zspeed;
				transform.Translate (x, 0, 0);
				transform.Translate (0, 0, z);
			} else {
				var x = Input.GetAxis ("Horizontal") * Time.deltaTime * (Xspeed * Runspeed);
				var z = Input.GetAxis ("Vertical") * Time.deltaTime * (Zspeed * Runspeed);
				transform.Translate (x, 0, 0);
				transform.Translate (0, 0, z);
			}

			if (Input.GetKey (KeyCode.LeftShift)) {
				RUN = true;
			} else {
				RUN = false;
			}

			if (UP) {
				DOWN = false;
				RIGHT = false;
				LEFT = false;
			}
			if (DOWN) {
				UP = false;
				RIGHT = false;
				LEFT = false;
			}
			if (RIGHT) {
				DOWN = false;
				UP = false;
				LEFT = false;
			}
			if (LEFT) {
				DOWN = false;
				RIGHT = false;
				UP = false;
			}

			if ((Input.GetKey (KeyCode.UpArrow)) && (ShootSpeed == ShootSpeedRecarga) && ((!UP) && (!DOWN) && (!LEFT) && (!RIGHT))) {
				CmdDisparadoAdelante ();
				Disparado = true;
				UP = true;
			}
			if ((Input.GetKey (KeyCode.RightArrow)) && (ShootSpeed == ShootSpeedRecarga) && ((!RIGHT) && (!LEFT) && (!UP) && (!DOWN))) {
				CmdDisparadoDerecha ();
				Disparado = true;
				RIGHT = true;
			}
			if ((Input.GetKey (KeyCode.DownArrow)) && (ShootSpeed == ShootSpeedRecarga) && (!DOWN) && (!UP) && (!RIGHT) && ((!LEFT))) {
				CmdDisparadoAtras ();
				Disparado = true;
				DOWN = true;
			}
			if ((Input.GetKey (KeyCode.LeftArrow)) && (ShootSpeed == ShootSpeedRecarga) && ((!LEFT) && (!RIGHT) && (!DOWN) && (!UP))) {
				CmdDisparadoIzquierda ();
				Disparado = true;
				LEFT = true;
			}
		} else {
			return;
		}
	}

	[Command]
	void CmdDisparadoAdelante(){
		if (clase == 0) {
			var bullet = (GameObject)Instantiate (BulletPref, new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z + 1), new Quaternion (this.gameObject.transform.rotation.x, this.gameObject.transform.rotation.y, this.gameObject.transform.rotation.z, 0));
			Bala_sc = bullet.GetComponent<Bala> ();
			Bala_sc.myPlayer = this.gameObject;
			if (!RUN) {
				bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * shootforce;
			} else {
				var shootforceRunning = Runspeed * shootforce/1.25f;
				bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * shootforceRunning;
			}
			NetworkServer.Spawn (bullet);
		}
	}

	[Command]
	void CmdDisparadoAtras(){
		if (clase == 0) {
			var bullet = (GameObject)Instantiate (BulletPref, new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z - 1), new Quaternion (this.gameObject.transform.rotation.x, this.gameObject.transform.rotation.y - 180, this.gameObject.transform.rotation.z, 0));
			Bala_sc = bullet.GetComponent<Bala> ();
			Bala_sc.myPlayer = this.gameObject;
			if (!RUN) {
				bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * shootforce;
			} else {
				var shootforceRunning = Runspeed * shootforce/1.25f;
				bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * shootforceRunning;
			}
			NetworkServer.Spawn (bullet);
		}
	}

	[Command]
	void CmdDisparadoDerecha(){
		if (clase == 0) {
			var bullet = (GameObject)Instantiate (BulletPref, new Vector3 (this.transform.position.x + 1, this.transform.position.y, this.transform.position.z), new Quaternion (this.gameObject.transform.rotation.x, this.gameObject.transform.rotation.y, this.gameObject.transform.rotation.z, 0));
			Bala_sc = bullet.GetComponent<Bala> ();
			Bala_sc.myPlayer = this.gameObject;
			if (!RUN) {

				bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.right * shootforce;
			}else{
				var shootforceRunning = Runspeed * shootforce/1.25f;
				bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.right * shootforceRunning;
			}
			NetworkServer.Spawn (bullet);
		}
	}

	[Command]
	void CmdDisparadoIzquierda(){
		if (clase == 0) {
			var bullet = (GameObject)Instantiate (BulletPref, new Vector3 (this.transform.position.x - 1, this.transform.position.y, this.transform.position.z), new Quaternion (this.gameObject.transform.rotation.x, this.gameObject.transform.rotation.y - 90, this.gameObject.transform.rotation.z, 0));
			Bala_sc = bullet.GetComponent<Bala> ();
			Bala_sc.myPlayer = this.gameObject;
			if (!RUN) {
				bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.right * shootforce;
			}else{
				var shootforceRunning = Runspeed * shootforce/1.25f;
				bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.right * shootforceRunning;
			}
			NetworkServer.Spawn (bullet);
		}
	}
}
